
# OH-RISCV-SIG
## 公告

* 本仓为sig-riscv 组芯片启动任务的代码仓，用来存放任务相关的代码和技术资料
* 本SIG组例会会议纪要文件请参阅此[链接](https://gitee.com/openharmony-sig/sig-content/tree/master/riscv)
* [三方库相关任务列表](./docs/devel/三方库贡献列表.md)

## 综合看板

|              |                                                              |                                                              |  |
| :----------: | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------- |
| **负责单位** |                       **当前工作方向**                       |                         **具体进度**                         | **未来工作方向** |
| 中科院软件所 | 基于QEMU_RISCV64平台完成OpenHarmony 3.2Release系统版本的适配 | 已适配：内核、网络、图形显示、input、音频；<br />适配中：Camera<br/> | 基于QEMU_RISCV64平台，完成后续OpenHarmony版本升级，OpenHarmony for RISCV能力优化 |
|     润和     | 基于DAYU800平台完成OpenHarmony 主线版本的适配 | 正在解决master主线的基础环境编译问题：<br />已完成：build+musl仓编译适配<br />适配中：third_party三方库、arkcompiler、rust等 | 解决基础环境问题后，分别开始RSIC-V 架构代码有关工作、第三方库支持、内核适配及外设适配。解决完成主干代码适配后，投入行业应用场景（如云桌面、工业网关及STB） |
|     上海交通大学     | 构建适配OpenHarmony的RISC-V可信执行环境，蓬莱-OH | 已适配：目前已经能够在OpenHarmony Qemu环境（不包含图形等）中运行蓬莱，并支持运行蓬莱开源SDK提供的主要Demo应用<br />进行中：适配最新OpenHarmony RISC-V 3.2实现 | 和OpenHarmony RISC-V一起合入主线版本（基于4.0及后续版本） |
| 上海交通大学 | 构建分布式可信执行环境（TEE），Distributed TEE系统，支持多设备TEE安全能力共享和打通 | 已适配：基于OpenHarmony RISC-V Qemu环境、赛昉RISC-V开发板和蓬莱可信执行环境，目前已经能够支持跨设备使用TEE硬件能力原型，并且通过ARCH SIG评审在OpenHarmony SIG中建仓孵化；<br />进行中：整合分布式TEE项目的代码和文档，尝试接入OpenHarmony的分布式软总线系统 | 结合OpenHarmony的分布式软总线系统，完成项目孵化并开始合入主线 |
| 中国科学院空天信息创新研究院 | 基于DAYU800平台开展OpenHarmony主线中third_party三方库适配 | 已适配：bzip2,libusb<br />进行中：mindspore、openss、grpcl等 | 结合主线仓库构建需求，开展third_party三方库适配 |




